function promedio(arrayObject){
    var divisor = arrayObject.length;
    if(divisor==0){
        return 'error'
    }
    var sumatoria = 0

    for(var i=0; i< divisor; i++){
        sumatoria += arrayObject[i]
    }
    return (sumatoria/divisor)
}

module.exports.promedio = promedio

