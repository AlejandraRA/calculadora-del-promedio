var assert = require('assert');
var operaciones = require('../operaciones.js')

describe('Array', function () {
  describe('#indexOf()', function () {
    it('should return -1 when the value is not present', function () {
      assert.equal([1, 2, 3].indexOf(4), -1);
    });
  });
});


/* describe('boolean', function () {
  it('should return true when no empty', function () {
    assert.equal('hola kodemia'==true, true)
  })
}) */

describe('Promedio', function () {
  describe('cuando todos los numeros sean positivos', function () {
    it('deberia calcular el promedio', function () {
      assert.equal(operaciones.promedio([1, 2, 3, 4]), 2.5)
    })
  })
  describe('numeros mixtos', function () {
    it('deberia calcular promedio de mixtos', function () {
      assert.equal(operaciones.promedio([-5, 20]), 7.5)
    })
  })
  describe('numeros vacios', function () {
    it('Deberia regresar un string que diga error', function () {
      assert.equal(operaciones.promedio([]), 'error')
    })
  })
  describe('cuando el arreglo es de una cadena', function () {
    it('Deberia regresar error', function () {
      assert.equal(operaciones.promedio([]), 'error')
    })
  })
})

